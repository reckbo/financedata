# What is it?

A pipeline that saves text versions of the financial tables on MSN money
(balance sheet, income statement, statement of cash flow).

# Dependencies

- redo (https://github.com/mildred/redo)
- w3m
- curl, egrep, sed
- hxselect (html-xml-utils)

# How to run

    redo AAPL.bal.txt  # gets Apple's balance sheet
    redo AAPL.cash.txt  # gets Apple's cash flow
    redo AAPL.inc.txt  # gets Apple's income statement

Here's an example on how to get the balance sheets from a list of NYSE stocks 
(4 in parallel)

    for i in `head NYSE`; do echo $i.bal.txt; done | xargs redo -j4
